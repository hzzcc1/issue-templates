# Issue Templates
These are some examples of issue templates which can be used to populate issues within GitLab.
Feel free to use some/all/none of them as part of your projects within GitLab.

The templates need to exist within the `.gitlab/issue_templates` for them to be available for selection when creating a new issue.
